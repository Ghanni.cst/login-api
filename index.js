const express    = require("express");
const login = require('./routes/login');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
const router = express.Router();
// test route
app.get("/",(req,res)=>res.json(`Welcome To Our DataBase`));

//route to handle user registration
router.post('/register',login.register);
router.post('/login',login.login)
app.use('/api', router);

const port = process.env.PORT || 6000;
app.listen(port, () => console.log(`Listening on port ${port}...`));
